function checkForEmail() {
  var successSubject = "Scheduled podcast-generator execution 100% success";
  var errorSubject = "Scheduled podcast-generator execution finished with errors";
  var userEmail = 'adpabloslopez@gmail.com';
  var fromEmail = 'alex@alexdepablos.com';
  var deadlineHour = 10; // The hour of the day when you want to receive the alert (24-hour format)

  var now = new Date();
  var startOfDay = new Date(now.getFullYear(), now.getMonth(), now.getDate());

  // Skip weekends
  if (now.getDay() == 0 || now.getDay() == 6) {
    return;
  }

  var querySuccess = 'from:' + fromEmail + ' subject:' + successSubject + ' after:' + startOfDay.toISOString().slice(0,10);
  var queryError = 'from:' + fromEmail + ' subject:' + errorSubject + ' after:' + startOfDay.toISOString().slice(0,10);

  var threadsSuccess = GmailApp.search(querySuccess);
  var threadsError = GmailApp.search(queryError);

  var properties = PropertiesService.getScriptProperties();
  var alertedToday = properties.getProperty('alertedToday');

  if (now.getHours() >= deadlineHour && threadsSuccess.length == 0 && threadsError.length == 0 && alertedToday != startOfDay.toISOString()) {
    var currentTime = Utilities.formatDate(now, Session.getScriptTimeZone(), 'HH:mm');
    GmailApp.sendEmail(userEmail, 'Alert: no email received', `
      At ${currentTime}, no email with the subject "${successSubject}" or "${errorSubject}" has been received from ${fromEmail}.
    `);
    properties.setProperty('alertedToday', startOfDay.toISOString());
  }
}

// This function checks the email every hour
function hourlyEmailCheck() {
  ScriptApp.newTrigger('checkForEmail')
    .timeBased()
    .everyHours(1)
    .inTimezone('America/New_York') // Set to Eastern Time
    .create();
}

// This function is executed once a day to delete the 'alertedToday' property
function dailyReset() {
  var properties = PropertiesService.getScriptProperties();
  properties.deleteProperty('alertedToday');
}

// This function creates a daily trigger to reset the 'alertedToday' property
function dailyResetTrigger() {
  ScriptApp.newTrigger('dailyReset')
    .timeBased()
    .everyDays(1)
    .atHour(0)
    .inTimezone('America/New_York') // Set to Eastern Time
    .create();
}

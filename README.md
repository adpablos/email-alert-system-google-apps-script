# Email Alert System Google Apps Script

This repository contains the code for an email alert system built with Google App Script. The system monitors a specific email inbox and sends an alert if a certain email is not received by a specified time each day.

## How It Works

The script checks the inbox for an email with a specific subject line. If the email is not found by a certain time, the script sends an alert email to a specified email address.

The script is set to run every hour, but this can be adjusted according to your needs.

## Setup

1. Copy the code from `EmailAlerts.gs` file.
2. Go to [Google App Script](https://script.google.com/) and create a new project.
3. Paste the copied code into the script editor.
4. Replace the placeholders in the code with your specific details (email addresses, subject line, etc.).
5. Save the project and run the `hourlyEmailCheck()` function to start the email monitoring.

## Stopping the Alerts

If you want to stop the email alerts, you can do so by going to the Triggers page in Google App Script and deleting the corresponding triggers.

## Contributing

If you have suggestions for how this script could be improved, feel free to open an issue or a pull request.

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details.
